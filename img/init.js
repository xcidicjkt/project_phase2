 $(document).ready(function() {
            var TOTAL_SLIDES = 7;
            $('#fullpage').fullpage({
                css3 : true,
                anchors:['home', 'about', 'what', 'works', 'culture', 'contact'],
                autoScrolling: false,
                scrollbar : true,
                fitToSection: false,
                fitToSectionDelay: 0,
                verticalCentered: false,
                loopHorizontal: false,

                afterLoad: function(anchorLink, index){
                    $('#slide-0 .body-content').addClass('show-content');
                },

                afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
                    for (var i = 0; i < TOTAL_SLIDES; i++) {
                      $('#slide-' + i).removeClass('rotate-left-min');
                      $('#slide-' + i).removeClass('rotate-left-max');
                      $('#slide-' + i).removeClass('rotate-right-min');
                      $('#slide-' + i).removeClass('rotate-right-max');                     
                    }
                      $('#slide-' + slideIndex + ' .body-content').addClass('show-content');
                      $titlename = ['', 'about us', 'what we do', 'our works', 'our culture', 'contact us'],
                      $('.title h3').text($titlename[slideIndex]);
                  }, 

                  onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
                    var leavingSlide = $(this);
                    var enteringSlide = $('#slide-' + nextSlideIndex);

                    //rotate right
                    if (direction === 'right') {
                      $(leavingSlide).addClass('rotate-left-min');
                      $(enteringSlide).addClass('rotate-right-min');
                    }

                    // rotate left
                    if (direction === 'left') {
                      $(leavingSlide).addClass('rotate-right-max');
                      $(enteringSlide).addClass('rotate-left-max');
                    }

                    $('#slide-' + slideIndex + ' .body-content').removeClass('show-content');
                  }
            }); 
            $( ".about-button" ).click(function() {
              $( ".half-content" ).toggle();
            });   
            $( ".culture-button1" ).click(function() {
              $( ".sub1" ).show();
              $( ".sub" ).hide();
            });
            $( ".culture-button2" ).click(function() {
              $( ".sub2" ).show();
              $( ".sub" ).hide();
            }); 
            $( ".culture-button3" ).click(function() {
              $( ".sub3" ).show();
              $( ".sub" ).hide();
            }); 
            $( ".culture-button4" ).click(function() {
              $( ".sub4" ).show();
              $( ".sub" ).hide();
            }); 
            $( ".close-button1" ).click(function() {
              $( ".sub1" ).hide();
              $( ".sub" ).show();
            });
            $( ".close-button2" ).click(function() {
              $( ".sub2" ).hide();
              $( ".sub" ).show();
            }); 
            $( ".close-button3" ).click(function() {
              $( ".sub3" ).hide();
              $( ".sub" ).show();
            }); 
            $( ".close-button4" ).click(function() {
              $( ".sub4" ).hide();
              $( ".sub" ).show();
            });         
        });

