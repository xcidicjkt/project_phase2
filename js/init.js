 $(document).ready(function() {


            var TOTAL_SLIDES = 7;
            $('#fullpage').fullpage({
                css3 : true,
                anchors:['home', 'about', 'what', 'works', 'culture', 'testimonial', 'career', 'contact'],
                autoScrolling: true,
                scrollbar : true,
                fitToSection: false,
                fitToSectionDelay: 0,
                verticalCentered: false,
                loopHorizontal: true,
                controlArrows: true,

                afterLoad: function(anchorLink, index){
                    $('#slide-0 .body-content').addClass('show-content');
                },

                afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
                    for (var i = 0; i < TOTAL_SLIDES; i++) {
                      $('#slide-' + i).removeClass('rotate-left-min');
                      $('#slide-' + i).removeClass('rotate-left-max');
                      $('#slide-' + i).removeClass('rotate-right-min');
                      $('#slide-' + i).removeClass('rotate-right-max');                     
                    }
                      $('#slide-' + slideIndex + ' .body-content').addClass('show-content');

                      $('.nav__item.active').removeClass('active');
                      $('.nav__item').eq(slideIndex).addClass('active');

                      $title = ["", "About Us", "What We Do", "Our Works", "Our Culture", "Testimonial", "Career", "Contact Us"];
                      $('.title h1').text($title[slideIndex]);

                      if (slideIndex === 0) {
                         $('.footer3').css('display', 'none');
                       $('.footer-ipad h2').html('We exist to make difference to people\'s life'); 
                        $('.footer2 h2').text(''); 
                      } else if(slideIndex === 3) {
                        $('.footer3').css('visibility', 'visible');
                        $('.footer3').css('display', 'block');
                        $('.footer-ipad h2').text('');
                      }else {
                        $('.footer-ipad h2').text('');
                        $('.footer3').css('visibility', 'hidden');
                      }
                  }, 

                  onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
                    var leavingSlide = $(this);
                    var enteringSlide = $('#slide-' + nextSlideIndex);

                    //rotate right
                    if (direction === 'right') {
                      $(leavingSlide).addClass('rotate-left-min');
                      $(enteringSlide).addClass('rotate-right-min');
                    }

                    // rotate left
                    if (direction === 'left') {
                      $(leavingSlide).addClass('rotate-right-max');
                      $(enteringSlide).addClass('rotate-left-max');
                    }

                    $('#slide-' + slideIndex + ' .body-content').removeClass('show-content');
                  }
            }); 
             
            
            $.material.init();
        });
 // makes sure the whole site is loaded
 $(window).on('load', function() {
  $("#spinner").fadeOut();
  $("#pre-loader").delay(1500).fadeOut("slow");
 });

 $(document).ready(function () {
   var trigger = $('.hamburger'),
      trigger1 = $('.back'),
      overlay1 = $('.overlay1'),
      isClosed = false;

     trigger.click(function () {
       hamburger_cross();      
     });
     overlay1.click(function () {
       hamburger_cross();      
     });
     $('li .menu').click(function () {
       hamburger_cross();      
     });

     function hamburger_cross() {

       if (isClosed == true) {          
         overlay1.hide();
         trigger.removeClass('is-open');
         trigger.addClass('is-closed');
         isClosed = false;
       } else {   
         overlay1.show();
         trigger.removeClass('is-closed');
         trigger.addClass('is-open');
         isClosed = true;
       }
   }
   
   $('[data-toggle="offcanvas"]').click(function () {
         $('#wrapper1').toggleClass('toggled');
   }); 
   $('[data-toggle="offcanvas1"]').click(function () {
         $('#wrapper2').toggleClass('toggled');
   });  

   if($(window).width() >= 800) {

     $( "#slide-3 .bg1" ).hover(
       function() {
         $( "#slide-3 .bg1-highlight" ).css("background-image", "url(img/03h-1.png)");  
       }, function(){
         $( "#slide-3 .bg1-highlight" ).css("background-image", "none");
      }
     );
     $( "#slide-3 .bg2" ).hover(
       function() {
         $( "#slide-3 .bg2-highlight" ).css("background-image", "url(img/03h-4.png)");  
       }, function(){
         $( "#slide-3 .bg2-highlight" ).css("background-image", "none");
      }
     );
     $( "#slide-3 .bg3" ).hover(
       function() {
         $( "#slide-3 .bg3-highlight" ).css("background-image", "url(img/03h-2.png)");  
       }, function(){
         $( "#slide-3 .bg3-highlight" ).css("background-image", "none");
      }
     );
     $( "#slide-3 .bg4" ).hover(
       function() {
         $( "#slide-3 .bg4-highlight" ).css("background-image", "url(img/03h-3.png)");  
       }, function(){
         $( "#slide-3 .bg4-highlight" ).css("background-image", "none");
      }
     );
   }
 });

 $(document).ready(function (){
     validate();
     $('#email, #enquiry').change(validate);
     validate1();
     $('#email1, #enquiry1').change(validate1);

     $('form[name=desktop-form]').submit(function(event) {
        var service_id = 'gmail';
        var template_id = 'xcidic_template';
        var template_params = {
          name: $('input[name=email]').val(),
          message: $('textarea[name=enquiry]').val()
        };

        emailjs.send(service_id,template_id,template_params)
        .then(function(response) {
           console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
           $('#contact_button').button('reset');
           swal({
            title: "Success!", 
            text: "Your message was sent successfully. Thanks.", 
            type: "success",
            allowOutsideClick: true
          });
           $('input[name=email]').val("");
           $('textarea[name=enquiry]').val("");
        }, function(err) {
           console.log("FAILED. error=", err);
           $('#contact_button').button('reset');
           swal({
             title: "Error!", 
             text: "Failed to send your message. Please try again.", 
             type: "error",
             allowOutsideClick: true
            });
        });
        event.preventDefault();
     });

     $('#contact_button').on('click', function() {
         var $this = $(this);
         $this.button('loading');
     });

     $('form[name=mobile-form]').submit(function(event) {

        var service_id = 'gmail';
        var template_id = 'xcidic_template';
        var template_params = {
          name: $('input[name=email1]').val(),
          message: $('textarea[name=enquiry1]').val()
        };

        emailjs.send(service_id,template_id,template_params)
        .then(function(response) {
           console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
           $('#contact_button1').button('reset');
           swal({
            title: "Success!", 
            text: "Your message was sent successfully. Thanks.", 
            type: "success",
            allowOutsideClick: true
          });
           $('input[name=email1]').val("");
           $('textarea[name=enquiry1]').val("");
           $('#wrapper2').toggleClass('toggled');
        }, function(err) {
           console.log("FAILED. error=", err);
           $('#contact_button1').button('reset');
           swal("Error!", "Failed to send your message. Please try again.", "error");
            swal({
             title: "Error!", 
             text: "Failed to send your message. Please try again.", 
             type: "error",
             allowOutsideClick: true
            });
        });
        event.preventDefault();
     });
      $('#contact_button1').on('click', function() {
         var $this = $(this);
         $this.button('loading');
     });
 });

 function validate(){
     if (isEmail($('#email').val())   &&
         $('#enquiry').val().length  >   0   ) {
         $("#contact_button").prop("disabled", false);
     }
     else {
         $("#contact_button").prop("disabled", true);
     }
 }

 function validate1(){
     if (!isEmail($('#email1'))   &&
         $('#enquiry1').val().length  >   0   ) {
         $("#contact_button1").prop("disabled", false);
     }
     else {
         $("#contact_button1").prop("disabled", true);
     }
 }

 function isEmail(email) {
   var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   return regex.test(email);
 }

  $(document).ready(function () {
    $( "#slide-2 .about-button" ).click(function() {
      $( "#slide-2 .half-content" ).toggle();
    });

    $( "#slide-5 .testimonial-button" ).click(function() {
      $( "#slide-5 .half-content" ).toggle();
    });  

    $( "#slide-5 .testimonial-mb-button" ).click(function() {
      $( "#slide-5 .half-mb-content" ).toggle();
    }); 

    $( "#slide-4 .open-culture1" ).click(function() {
      $( "#slide-4 .sub-detail1" ).show();
      $( "#slide-4 .sub" ).hide();
    });
    $( "#slide-4 .open-culture2" ).click(function() {
      $( "#slide-4 .sub-detail2" ).show();
      $( "#slide-4 .sub" ).hide();
    }); 
    $( "#slide-4 .open-culture3" ).click(function() {
      $( "#slide-4 .sub-detail3" ).show();
      $( "#slide-4 .sub" ).hide();
    }); 
    $( "#slide-4 .open-culture4" ).click(function() {
      $( "#slide-4 .sub-detail4" ).show();
      $( "#slide-4 .sub" ).hide();
    }); 
    $( "#slide-4 .close-culture1" ).click(function() {
      $( "#slide-4 .sub-detail1" ).hide();
      $( "#slide-4 .sub" ).show();
    });
    $( "#slide-4 .close-culture2" ).click(function() {
      $( "#slide-4 .sub-detail2" ).hide();
      $( "#slide-4 .sub" ).show();
    }); 
    $( "#slide-4 .close-culture3" ).click(function() {
      $( "#slide-4 .sub-detail3" ).hide();
      $( "#slide-4 .sub" ).show();
    }); 
    $( "#slide-4 .close-culture4" ).click(function() {
      $( "#slide-4 .sub-detail4" ).hide();
      $( "#slide-4 .sub" ).show();
    });

    $( "#slide-6 .open-career1" ).click(function() {
      $( "#slide-6 .sub-detail1" ).show();
      $( "#slide-6 .sub" ).hide();
    });
    $( "#slide-6 .open-career2" ).click(function() {
      $( "#slide-6 .sub-detail2" ).show();
      $( "#slide-6 .sub" ).hide();
    }); 
    $( "#slide-6 .close-career1" ).click(function() {
      $( "#slide-6 .sub-detail1" ).hide();
      $( "#slide-6 .sub" ).show();
    });
    $( "#slide-6 .close-career2" ).click(function() {
      $( "#slide-6 .sub-detail2" ).hide();
      $( "#slide-6 .sub" ).show();
    }); 

    $( "#slide-6 .open-mb-career1" ).click(function() {
      $( "#slide-6 .sub-mb-detail1" ).show();
      $( "#slide-6 .sub-mb" ).hide();
    });
    $( "#slide-6 .open-mb-career2" ).click(function() {
      $( "#slide-6 .sub-mb-detail2" ).show();
      $( "#slide-6 .sub-mb" ).hide();
    }); 
    $( "#slide-6 .close-mb-career1" ).click(function() {
      $( "#slide-6 .sub-mb-detail1" ).hide();
      $( "#slide-6 .sub-mb" ).show();
    });
    $( "#slide-6 .close-mb-career2" ).click(function() {
      $( "#slide-6 .sub-mb-detail2" ).hide();
      $( "#slide-6 .sub-mb" ).show();
    }); 
});
 


